package model.logic;

import java.awt.BorderLayout;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.InfoWindow;
import com.teamdev.jxmaps.InfoWindowOptions;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Marker;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.*;

public class Maps extends MapView {

	// Objeto Google Maps
	private Map map;
	private CircleOptions settingsCircle;
	private PolylineOptions settingsLine;
	private GrafNoDirigido grafo = new GrafNoDirigido<>();

	public Maps(GrafNoDirigido pGrafo)
	{	
		grafo=pGrafo;
		settingsCircle=new CircleOptions();
		settingsCircle.setFillColor("#FF0000");
		settingsCircle.setRadius(10);
		settingsCircle.setFillOpacity(0.35);

		settingsLine=new PolylineOptions();
		settingsLine.setGeodesic(true);
		settingsLine.setStrokeColor("#FF0000");
		settingsLine.setStrokeOpacity(1.0);
		settingsLine.setStrokeWeight(2.0);

		setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				// Check if the map is loaded correctly
				if (status == MapStatus.MAP_STATUS_OK) {
					// Getting the associated map object
					map = getMap();
					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					controlOptions.setPosition(ControlPosition.BOTTOM_LEFT);
					mapOptions.setMapTypeControlOptions(controlOptions);

					map.setOptions(mapOptions);
					map.setZoom(10);

					generarArcos();

					initMap( map );
				}
			}
		});


	}

	public Maps(GrafNoDirigido pGrafo, InfoVertice info1, InfoVertice info2)
	{	
		grafo=pGrafo;
		settingsCircle=new CircleOptions();
		settingsCircle.setFillColor("#FF0000");
		settingsCircle.setRadius(10);
		settingsCircle.setFillOpacity(0.35);

		settingsLine=new PolylineOptions();
		settingsLine.setGeodesic(true);
		settingsLine.setStrokeColor("#FF0000");
		settingsLine.setStrokeOpacity(1.0);
		settingsLine.setStrokeWeight(2.0);

		setOnMapReadyHandler( new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status)
			{
				if ( status == MapStatus.MAP_STATUS_OK )
				{
					map = getMap();

					//Vertice #1
					Marker nuevoMarcador = new Marker(map);	//Pone el marcador en el mapa						
					LatLng coordenadas = new LatLng(info1.darLatitud(), info1.darLongitud()); //Coordenadas Latitud-longitud
					nuevoMarcador.setPosition(coordenadas); //Configura y guarda la posicion											

					//Vertice #2
					Marker nuevoMarcador2 = new Marker(map);	//Pone el marcador en el mapa						
					LatLng coordenadas2 = new LatLng(info2.darLatitud(), info2.darLongitud()); //Coordenadas Latitud-longitud
					nuevoMarcador2.setPosition(coordenadas2); //Configura y guarda la posicion

					generarArcos();

					initMap( map );
				}
			}

		} );


	}

	public void initMap(Map map)
	{
		MapOptions mapOptions = new MapOptions();
		MapTypeControlOptions controlOptions = new MapTypeControlOptions();
		controlOptions.setPosition(ControlPosition.BOTTOM_LEFT);
		mapOptions.setMapTypeControlOptions(controlOptions);

		map.setOptions(mapOptions);
		map.setCenter(new LatLng(4.597714,-74.094723));
		map.setZoom(10.0);

	}

	public void initFrame(String titulo)
	{
		JFrame frame = new JFrame(titulo);
		frame.setSize(800, 800);
		frame.add(this, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}


	public void generarArcos()
	{
		Iterable<Integer> llaves = grafo.tablaHashVertices.keys();
		for (Integer i : llaves) {
			Queue<Arco> cola = (Queue<Arco>) grafo.tablaHashArcos.get(i);
			if(cola!=null) {
				for (Arco arco : cola) {
					InfoVertice infoVer1 = (InfoVertice) arco.darVer2().darInfoVer();
					InfoVertice infoVer2 = (InfoVertice) arco.darVer1().darInfoVer();
					generateSimplePath(new LatLng(infoVer1.darLatitud(), infoVer1.darLongitud()), new LatLng(infoVer2.darLatitud(), infoVer2.darLongitud()));
				}
			}
		}
	}

	public Maps(Iterable<Arco> arcos){
		settingsCircle=new CircleOptions();
		settingsCircle.setFillColor("#FF0000");
		settingsCircle.setStrokeColor("#4682B4");
		settingsCircle.setRadius(50);
		settingsCircle.setFillOpacity(0.35);

		settingsLine=new PolylineOptions();
		settingsLine.setGeodesic(true);
		settingsLine.setStrokeColor("#FF0000");
		settingsLine.setStrokeOpacity(1.0);
		settingsLine.setStrokeWeight(2.0);

		// Setting of a ready handler to MapView object. onMapReady will be called when map initialization is done and
		// the map object is ready to use. Current implementation of onMapReady customizes the map object.
		setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				// Check if the map is loaded correctly
				if (status == MapStatus.MAP_STATUS_OK) {
					// Getting the associated map object
					map = getMap();
					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					controlOptions.setPosition(ControlPosition.BOTTOM_LEFT);
					mapOptions.setMapTypeControlOptions(controlOptions);

					map.setOptions(mapOptions);
					map.setZoom(10);

					generarArcosReq1C(arcos);

					initMap( map );
				}
			}
		});

	}

	public void generarArcosReq1C(Iterable<Arco> arcos){
		int contador = 0;
		Iterable<Arco> arquitos = arcos;
		for (Arco arcotes : arquitos){
			if(contador == 1000) {
				break;
			}
			InfoVertice infoVer1 = (InfoVertice) arcotes.darVer2().darInfoVer();
			InfoVertice infoVer2 = (InfoVertice) arcotes.darVer1().darInfoVer();
			generateSimplePath(new LatLng(infoVer1.darLatitud(), infoVer1.darLongitud()), new LatLng(infoVer2.darLatitud(), infoVer2.darLongitud()));
			generateArea(new LatLng(infoVer1.darLatitud(), infoVer1.darLongitud()));
			generateArea(new LatLng(infoVer2.darLatitud(), infoVer2.darLongitud()));
			contador++;
		}
	}

	public void GenerateLine(LatLng... path)
	{
		Polyline polyline = new Polyline(map);
		polyline.setPath(path);
	}
	public void generateArea(LatLng center)
	{
		Circle circle = new Circle(map);
		circle.setCenter(center);
		circle.setRadius(0.0001);
		circle.setVisible(true);
		circle.setOptions(settingsCircle);
	}
	public void generateSimplePath(LatLng start,LatLng end)
	{
		LatLng[] path = {start,end};
		Polyline polyline = new Polyline(map);
		polyline.setPath(path);
	}



	public CircleOptions getSettingsCircle() {
		return settingsCircle;
	}

	public void setSettingsCircle(CircleOptions settingsCircle) {
		this.settingsCircle = settingsCircle;
	}


	public Maps(Queue<InfoVertice> vertices){
		setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				// Check if the map is loaded correctly
				if (status == MapStatus.MAP_STATUS_OK) {
					// Getting the associated map object
					map = getMap();
					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					controlOptions.setPosition(ControlPosition.BOTTOM_LEFT);
					mapOptions.setMapTypeControlOptions(controlOptions);

					map.setOptions(mapOptions);
					map.setZoom(10);

					generarVertices2(vertices);

					initMap( map );
				}
			}
		});
	}

	public void generarVertices2(Queue<InfoVertice> vertices){
		settingsCircle=new CircleOptions();
		settingsCircle.setFillColor("#BD2D0E");
		settingsCircle.setStrokeColor("#BD2D0E");
		settingsCircle.setRadius(20);
		settingsCircle.setFillOpacity(0.7);

		InfoVertice primero = vertices.dequeue();
		generateArea(new LatLng(primero.darLatitud(), primero.darLongitud()));

		settingsCircle=new CircleOptions();
		settingsCircle.setFillColor("#4682B4");
		settingsCircle.setStrokeColor("#4682B4");
		settingsCircle.setRadius(20);
		settingsCircle.setFillOpacity(0.7);

		for (InfoVertice inter: vertices){
			generateArea(new LatLng(inter.darLatitud(), inter.darLongitud()));
		}
	}

}