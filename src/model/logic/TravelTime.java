package model.logic;

import java.util.Comparator;

public class TravelTime implements Comparable<TravelTime>{
	
	
	private int sourceId;
	
	private int dstId;
	
	private int dow;
	
	private double meanTravelTime;
	
	private double standardDeviationTravelTime;
	
	private double geometricMeanTravelTime;  
	
	private double geometricStandardDeviationTravelTime;

	
	
	public TravelTime(int a, int b, int c, double d, double e, double f, double g) {
		sourceId=a;
		dstId=b;
		dow=c;
		meanTravelTime=d;
		standardDeviationTravelTime=e;
		geometricMeanTravelTime=f;
		geometricStandardDeviationTravelTime=g;
	
	}


	public int getSourceId() {
		return sourceId;
	}


	public int getDstId() {
		return dstId;
	}


	public int getDow() {
		return dow;
	}


	public double getMeanTravelTime() {
		return meanTravelTime;
	}


	public double getStandardDeviationTravelTime() {
		return standardDeviationTravelTime;
	}
	
	public double getGeometricMeanTravelTime() {
		return geometricMeanTravelTime;
	}
	
	public double getGeometricStandardDeviationTravelTime() {
		return geometricStandardDeviationTravelTime;
	}
	
	

	public static class comparatorInicial implements Comparator <TravelTime>
	{	@Override
	public int compare(TravelTime arg1, TravelTime arg2) {
		int retorno = (int) (arg1.getMeanTravelTime() - arg2.getMeanTravelTime());
        
		
		
			return retorno;
		

	}}

	@Override
	public int compareTo(TravelTime o) {
		// TODO Auto-generated method stub
		return 0;
	}


	
}
