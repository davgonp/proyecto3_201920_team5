package model.logic;


import java.awt.AlphaComposite;
import java.io.*;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.text.StyledEditorKit.ForegroundAction;

import com.opencsv.CSVReader;
import com.google.gson.*;
import model.data_structures.*;
import model.logic.Maps;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{
	/**
	 * Atributos del modelo del mundo
	 */



	private GrafNoDirigido<Integer,InfoVertice> grafo;

	private SeparateChainingHash<Integer, TravelTime> viajes;


	GrafNoDirigido<Integer, InfoVertice > grafoRTA;



	private int cantViaj;




	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		grafo=new GrafNoDirigido<Integer,InfoVertice>();
		viajes = new SeparateChainingHash<Integer, TravelTime>(5);
		grafoRTA = new GrafNoDirigido<Integer, InfoVertice >();
		cantViaj=0;


	}

	public GrafNoDirigido darGrafo() {
		return grafo;
	}


	public void cargar() {

		try 
		{ 

			CSVReader reader = null;
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-WeeklyAggregate.csv"));
			String[] nextLine=reader.readNext();



			while ((nextLine = reader.readNext()) != null) 
			{
				TravelTime elem = new TravelTime(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), 
						Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]),
						Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));

				viajes.put(Integer.parseInt(nextLine[0]), elem);
				cantViaj++;

			}
			reader.close();


			File f = new File("./data/bogota_vertices.txt");
			FileReader fr= new FileReader(f);
			BufferedReader bf= new BufferedReader(fr);
			bf.readLine();
			String efe = bf.readLine();


			while (efe!= null){  

				String[] datos = efe.split(";");
				InfoVertice infoV = new InfoVertice(Integer.parseInt(datos[0]),Double.parseDouble(datos[1]),Double.parseDouble(datos[2]),Integer.parseInt(datos[3]));
				grafo.addVertex(Integer.parseInt(datos[0]), infoV);
				efe=bf.readLine();

			}




			File f2 = new File("./data/bogota_arcos.txt");
			FileReader fr2= new FileReader(f2);
			BufferedReader bf2= new BufferedReader(fr2);
			String efe2 = bf2.readLine();

			while (efe2!=null){  

				String[] datos = efe2.split(" ");
				if(grafo.getInfoVertex(Integer.parseInt(datos[0])) !=null){

					double harv = 0.0;
					double time = 0.0;
					double vel = 0.0;

					double startLat=grafo.getInfoVertex(Integer.parseInt(datos[0])).darLatitud();
					double startLong=grafo.getInfoVertex(Integer.parseInt(datos[0])).darLongitud();

					for(int i =1; i< datos.length;i++) {
						if(grafo.getInfoVertex(Integer.parseInt(datos[i]))!=null) {


							double endLat=grafo.getInfoVertex(Integer.parseInt(datos[i])).darLatitud();
							double endLong=grafo.getInfoVertex(Integer.parseInt(datos[i])).darLongitud();


							double dLat  = Math.toRadians((endLat - startLat));
							double dLong = Math.toRadians((endLong - startLong));

							startLat = Math.toRadians(startLat);
							endLat   = Math.toRadians(endLat);

							double a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(startLat) * Math.cos(endLat) * Math.pow(Math.sin(dLong / 2), 2);
							double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

							harv= 6371 * c;




							if(grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber()==grafo.getInfoVertex(Integer.parseInt(datos[i])).darZonaUber()) {

								if(viajes.get(grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber())!=null && viajes.get(grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber()).getSourceId()==grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber() && viajes.get(grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber()).getDstId()==grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber()) {
									time = viajes.get(grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber()).getMeanTravelTime();
								}else {
									time = 10.0;
								}
							}



							else {

								if(viajes.get(grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber())!=null && viajes.get(grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber()).getSourceId()==grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber() && viajes.get(grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber()).getDstId()==grafo.getInfoVertex(Integer.parseInt(datos[i])).darZonaUber()) {
									time = viajes.get(grafo.getInfoVertex(Integer.parseInt(datos[0])).darZonaUber()).getMeanTravelTime();
								}else {
									time = 100.0;
								}

							}



							vel=harv/time;



							grafo.addEdge(Integer.parseInt(datos[0]), Integer.parseInt(datos[i]), harv, time, vel);

						}
					}
				}
				efe2=bf2.readLine();


			}




			FileWriter fNew= new FileWriter("./data/JSONmallaVial.json");
			Gson json1 = new GsonBuilder().create();
			json1.toJson(grafo,fNew);
			fNew.flush();
			fNew.close();
			bf.close();
			bf2.close();

		} 

		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}


		System.out.println("El número de vertices es: "+grafo.V()+ " y el número de arcos es: "+grafo.E());
		System.out.println("El número de viajes cargados es: "+ cantViaj);
	}

	public void cargarJSON() {
		Gson gson=new Gson();
		grafo = new GrafNoDirigido<Integer, InfoVertice>();
		try (BufferedReader br = new BufferedReader(new FileReader("./data/JSONmallaVial.json"))){
			String linea = br.readLine();
			grafo = gson.fromJson(linea, GrafNoDirigido.class);

		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		System.out.println("El número de vertices es: " + grafo.V() + " y el número de arcos es: " + grafo.E());
	}



	public double calcularHarv(double pLong1, double pLat1, double pLong2, double pLat2) {

		double harv = 0.0;

		double dLat  = Math.toRadians((pLat1 - pLat2));
		double dLong = Math.toRadians((pLong1 - pLong2));

		pLat1 = Math.toRadians(pLat1);
		pLat2 = Math.toRadians(pLat2);

		double a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(pLat1) * Math.cos(pLat2) * Math.pow(Math.sin(dLong / 2), 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		harv= 6371 * c;

		return harv;
	}



	public int ccGrafo() {
		return grafo.cc();
	}


	public int verticeMasCercano(double pLong, double pLat) {
		int rtaVer = 0;
		double costo1=0.0;
		double costoMin=Double.POSITIVE_INFINITY;
		for(int i = 0; i<=228045;i++) {
			if(grafo.getInfoVertex(i)!=null) {
				costo1= calcularHarv(grafo.getInfoVertex(i).darLongitud(), grafo.getInfoVertex(i).darLatitud(), pLong, pLat);
				if( costo1 < costoMin) {
					rtaVer=grafo.getInfoVertex(i).darId();
					costoMin=costo1;
				}
			}
		}

		return rtaVer;
	}



	public void caminoMenorCostoHarvesine(double pLongOrg, double pLatOrg, double pLongDst, double pLatDst) {
		int ver1 = verticeMasCercano(pLongOrg, pLatOrg);
		int ver2 = verticeMasCercano(pLongDst, pLatDst);

		Dijkstra dijk = new Dijkstra(grafo, ver1, 1);
		Stack<Integer> pila = dijk.getEjes(ver1, ver2);

		System.out.println("Costo mínimo: " + pila.pop());
		System.out.println("Camino: ");
		double distanciaAprox = 0;
		boolean primero = false;
		Vertice<String, InfoVertice> anterior  = null;

		GrafNoDirigido<Integer, InfoVertice> grafoTemp = new GrafNoDirigido<Integer, InfoVertice>();

		for (int key : pila) {
			Vertice<Integer, InfoVertice> vertex = grafo.getVertices().get(key);
			grafoTemp.addVertex(vertex.darIdVertice(), vertex.darInfoVer());
			System.out.println("Id: " + vertex.darIdVertice() + " - Ubicación geográfica: (" + vertex.darInfoVer().darLongitud() + ", " + vertex.darInfoVer().darLatitud() + ")");
		}

		int ant = 0;
		boolean primerElemento = false;

		for(int llaves : pila) {
			if(primerElemento == true) {

				double distancia = calcularHarv(grafoTemp.getVertices().get(ant).darInfoVer().darLongitud(), grafoTemp.getVertices().get(ant).darInfoVer().darLatitud(), grafoTemp.getVertices().get(llaves).darInfoVer().darLongitud(), grafoTemp.getVertices().get(llaves).darInfoVer().darLatitud());
				distanciaAprox += distancia;
				grafoTemp.addEdge(ant, llaves, distancia,0,0);
			}
			ant = llaves;
			primerElemento = true;
		}

		System.out.println("Distancia estimada: " + distanciaAprox + " Km");

		try{
			Maps maps = new Maps(grafoTemp, grafo.getVertices().get(ver1).darInfoVer(), grafo.getVertices().get(ver2).darInfoVer());
			maps.initFrame("Requerimiento");
		} catch (Exception e) {
			System.out.println("Mapa cargando correctamente.");
		}



	}


	public void verticesAlcanzablesTiempo(double pLong, double pLat, double pTiempo) {

		int ver1 = verticeMasCercano(pLong, pLat);

		Queue<InfoVertice> data = new Queue<InfoVertice>();
		data.enqueue(grafo.getInfoVertex(ver1));

		Iterator<Integer> f = grafo.adj(ver1);

		while(f.hasNext()) {
			Vertice<Integer, InfoVertice> w = grafo.tablaHashVertices.get(f.next());
			int vertice2 = w.darIdVertice();
			if(grafo.getCostArcTiempo(ver1, w.darIdVertice()) <= pTiempo) {
				System.out.println("Id: " + vertice2 + " - Ubicación geográfica: (" + grafo.getInfoVertex(vertice2).darLongitud() + ", " + grafo.getInfoVertex(vertice2).darLatitud() + ")");
				data.enqueue(grafo.getInfoVertex(vertice2));
			}
		}

		try{
			Maps maps = new Maps(data);
			maps.initFrame("Requerimiento");
		} catch (Exception e) {
			System.out.println("Mapa cargando correctamente.");
		}

	}


	public void mstDistancia() {
		
		long startTime = System.currentTimeMillis();
		GrafNoDirigido <Integer, InfoVertice> grafoTemp = new GrafNoDirigido <Integer, InfoVertice>();
		for (Integer inte : grafo.getCC(7)) {
			grafoTemp.addVertex(inte, grafo.getInfoVertex(inte));
			System.out.println("Id: "+ inte);

		}

		for (Integer llaves : grafoTemp.getVertices().keys()) {
			Queue<Arco> bf = grafo.getArcos().get(llaves);
			for (Arco arco : bf) {
				int idVerDes = (int) arco.darVer2().darIdVertice();
				if(grafoTemp.getVertices().contains(idVerDes)){
					grafoTemp.addEdge(llaves, idVerDes, arco.darCostoHarv(), arco.darCostoTiempo(), arco.darCostoVel());
				}
			}
		}

		System.out.println("El número de vertices es: " + grafoTemp.V());

		KruskalMST krus = new KruskalMST(grafoTemp);
		for (Arco arquitos : krus.edges()) {
			System.out.println("Id partida: " + (int)arquitos.darVer1().darIdVertice() + " - Id llegada: " + (int)arquitos.darVer2().darIdVertice());
		}
		System.out.println("Costo total: " + krus.getPeso()+ " km");
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;

		System.out.println("El algoritmo tarda " + duration + " milisegundos");
		try{
			Maps maps = new Maps(krus.edges());
			maps.initFrame("Requerimiento");
		} catch (Exception e) {
			System.out.println("Mapa cargando correctamente.");
		}

	}


	public void grafoZonas (){

		for (Integer llaves : grafo.getVertices().keys()) {
			Queue<Arco> bf = grafo.getArcos().get(llaves);
			if(bf != null) {
				for (Arco arco : bf) {
					int idVerDes = (int) arco.darVer2().darIdVertice();

					int zona1 = grafo.getInfoVertex(llaves).darZonaUber();
					int zona2 = grafo.getInfoVertex(idVerDes).darZonaUber();

					if(zona1 != zona2) {
						if(!grafoRTA.getVertices().contains(zona1) && !grafoRTA.getVertices().contains(zona2)) {
							grafoRTA.addVertex(zona1, grafo.getInfoVertex(llaves));
							grafoRTA.addVertex(zona2, grafo.getInfoVertex(idVerDes));
							grafoRTA.addEdge(zona1, zona2, 0, grafo.getCostArcTiempo(llaves, idVerDes), 0);
						}
						else if(grafoRTA.getVertices().contains(zona1) && !grafoRTA.getVertices().contains(zona2)) {
							grafoRTA.addVertex(zona2, grafo.getInfoVertex(idVerDes));
							grafoRTA.addEdge(zona1, zona2, 0, grafo.getCostArcTiempo(llaves, idVerDes), 0);

						}
						else if(!grafoRTA.getVertices().contains(zona1) && grafoRTA.getVertices().contains(zona2)) {
							grafoRTA.addVertex(zona1, grafo.getInfoVertex(llaves));
							grafoRTA.addEdge(zona1, zona2, 0, grafo.getCostArcTiempo(llaves, idVerDes), 0);
						}
						else if(grafoRTA.getVertices().contains(zona1) && grafoRTA.getVertices().contains(zona2)) {
							grafoRTA.addEdge(zona1, zona2, 0, grafo.getCostArcTiempo(llaves, idVerDes), 0);
						}
					}
				}
			}
		}

		System.out.println("Número de vértices: " + grafoRTA.V());
		System.out.println("Número de arcos: " + grafoRTA.E());
		try{
			Maps maps = new Maps(grafoRTA);
			maps.initFrame("Requerimiento");
		} catch (Exception e) {
			System.out.println("Mapa cargando correctamente.");
		}
	}


	public void costoMinimoZonasTiempo(int zonaOrg, int zonaDst) {

		long startTime = System.currentTimeMillis();
		Dijkstra dijk = new Dijkstra(grafo, zonaOrg, 0);
		Stack<Integer> pila = dijk.getEjes(zonaOrg, zonaDst);

		System.out.println("Costo mínimo: " + pila.pop());
		System.out.println("Camino: ");
		double tiempoAprox = 0;
		boolean primero = false;
		Vertice<String, InfoVertice> anterior  = null;

		GrafNoDirigido<Integer, InfoVertice> grafoTemp = new GrafNoDirigido<Integer, InfoVertice>();

		for (int key : pila) {
			Vertice<Integer, InfoVertice> vertex = grafo.getVertices().get(key);
			grafoTemp.addVertex(vertex.darIdVertice(), vertex.darInfoVer());
			System.out.println("Id: " + vertex.darIdVertice() + " - Ubicación geográfica: (" + vertex.darInfoVer().darLongitud() + ", " + vertex.darInfoVer().darLatitud() + ")");
		}

		int ant = 0;
		boolean primerElemento = false;

		for(int llaves : pila) {
			if(primerElemento == true) {

				double tiempo = grafo.getCostArcTiempo(ant, llaves);
				tiempoAprox += tiempo;
				grafoTemp.addEdge(ant, llaves, tiempo,0,0);
			}
			ant = llaves;
			primerElemento = true;
		}
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;

		System.out.println("Distancia estimada: " + tiempoAprox + " seg");
		System.out.println("El algoritmo tarda " + duration + " milisegundos");
		try{
			Maps maps = new Maps(grafoTemp, grafo.getVertices().get(zonaOrg).darInfoVer(), grafo.getVertices().get(zonaDst).darInfoVer());
			maps.initFrame("Requerimiento");
		} catch (Exception e) {
			System.out.println("Mapa cargando correctamente.");
		}
	}


	public void caminoMasLargo(int zonaOrg) {
		//		int[] data = new int[1160];
		//		for (Integer i : grafo.getArcos().keys()) {
		//			Queue <Arco> colaB = grafo.getArcos().get(i);
		//			for (Arco arco : colaB) {
		//				int zona1 = grafo.getInfoVertex((int) arco.darVer1().darIdVertice()).darZonaUber();
		//				int zona2 = grafo.getInfoVertex((int) arco.darVer2().darIdVertice()).darZonaUber();
		//				if(zona1 == zonaOrg && zona1 != zona2) {
		//					data[zona2] = data[zona2] + 1;
		//				}
		//			}
		//		}

		//		int posFinal = 0;
		//		int numMayor = 0;
		//		for(int i = 0; i < data.length; i++) {
		//			if(data[i] > numMayor){
		//				numMayor = data[i];
		//				posFinal = i;
		//			}
		//		}

		long startTime = System.currentTimeMillis();
		int tamanio = 1161;
		BFS bfs = new BFS(grafoRTA, zonaOrg, tamanio);

		Stack<InfoVertice> camino = bfs.darCaminoMasLejano();
		int cantidadVertices = camino.size();

		GrafNoDirigido<Integer, InfoVertice> caminoRta = new GrafNoDirigido<>();
		InfoVertice punto1 = camino.pop();

		System.out.println("El camino más largo tiene " + cantidadVertices + " vértices");
		System.out.println("El camino más largo tiene " + (cantidadVertices - 1) + " arcos");

		caminoRta.addVertex(punto1.darId(), punto1);
		while(!camino.isEmpty())
		{
			InfoVertice punto2 = camino.pop();
			caminoRta.addVertex(punto2.darId(), punto2);
			double tiempo = grafoRTA.getCostArcTiempo(punto1.darId(), punto2.darId());
			caminoRta.addEdge(punto1.darId(), punto2.darId(), 0, tiempo, 0);
			punto1 = punto2;
		}
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;

		for (Integer infoVertice : caminoRta.getVertices().keys()) {
			System.out.println(infoVertice);
		}
		System.out.println("El algoritmo tarda " + duration + " milisegundos");
		try{
			Maps maps = new Maps(caminoRta);
			maps.initFrame("Requerimiento");
		} catch (Exception e) {
			System.out.println("Mapa cargando correctamente.");
		}
	}



}