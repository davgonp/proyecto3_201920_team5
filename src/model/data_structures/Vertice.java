package model.data_structures;

public class Vertice <K,V> {

	private K idVertice;
	private V infoVer;
	private boolean marcado;


	public Vertice (K pIdVer,V pInfo) {
		idVertice=pIdVer;
		infoVer=pInfo;
		marcado = false;
	}


	public K darIdVertice () {
		return idVertice;
	}
	public V darInfoVer () {
		return infoVer;
	}
	public boolean estaMarcado() {
		return marcado;
	}
	public void cambiarMarcado(boolean pMarc) {
		marcado=pMarc;
	}

	
}
