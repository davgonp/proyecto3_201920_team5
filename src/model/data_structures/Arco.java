package model.data_structures;

public class Arco <K,V> implements Comparable<Arco>{

	private double costoHarv;
	private double costoTiempo;
	private double costoVelocidad;
	private Vertice<K,V> ver1;
	private Vertice<K,V> ver2;

	public Arco(Vertice<K,V> pVer1, Vertice<K,V> pVer2, double pCosto1, double pCosto2, double pCosto3){
		ver1 = pVer1;
		ver2 = pVer2;
		costoHarv = pCosto1;
		costoTiempo = pCosto2;
		costoVelocidad = pCosto3;
	}

	public Vertice<K,V> darVer1() {
		return ver1;
	}

	public Vertice<K,V> darVer2() {
		return ver2;
	}

	public double darCostoHarv() {
		return costoHarv;
	}

	public double darCostoTiempo() {
		return costoTiempo;
	}

	public double darCostoVel() {
		return costoVelocidad;
	}

	public void cambiarCosto(double pCos) {
		costoHarv = pCos;
	}
	
	public void cambiarCostoTiempo(double pCos) {
		costoTiempo = pCos;
	}

	public Vertice<K, V> other(Vertice<K, V> vertex) {
		if (vertex == ver1) {
			return ver2;
		}
		else if (vertex == ver2) {
			return ver1;
		}
		else {
			throw new IllegalArgumentException("Illegal endpoint");
		}
	}

	@Override
	public int compareTo(Arco arg0) {
		return Double.compare((Double) darCostoHarv(), (Double) arg0.darCostoHarv());
	}

}
