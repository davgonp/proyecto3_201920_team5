package model.data_structures;

import java.util.Iterator;

public class BFS {
	private static final int INFINITY = Integer.MAX_VALUE;
	private boolean[] marked; // is there an s-v path?
	private int[] edgeTo; // previous edge on shortest s-v path
	private int[] distTo; // number of edges shortest s-v path
	private int s;
	private int tamanio;
	private GrafNoDirigido<Integer, InfoVertice> grafo;

	public BFS(GrafNoDirigido<Integer, InfoVertice> grafoRTA, int s, int pTamanio)
	{
		grafo = grafoRTA;
		this.tamanio = pTamanio;
		marked = new boolean[tamanio];
		distTo = new int[tamanio];
		edgeTo = new int[tamanio];
		this.s = s;
		bfs(grafoRTA, s);
	}

	private void bfs(GrafNoDirigido<Integer, InfoVertice> G, int s){
		Queue<Integer> q = new Queue<Integer>();
		for (int v = 0; v < tamanio; v++)
			distTo[v] = 0;

		marked[s] = true;
		distTo[s] = 0;
		q.enqueue(s);
		while (!q.isEmpty()){
			int v = q.dequeue();

			Iterator<Integer> f = G.adj(v);
			int cont = 0;

			while(f.hasNext()) {
				Vertice<Integer, InfoVertice> w = G.tablaHashVertices.get(f.next());
				cont++;
			}

			Vertice<Integer, InfoVertice>[] data = new Vertice[cont];

			Iterator<Integer> g = G.adj(v);

			int cont2 = 0;
			while(g.hasNext()) {
				Vertice<Integer, InfoVertice> w = G.tablaHashVertices.get(g.next());
				data[cont2] = w;
				cont2++;
			}

			for (int i = 0; i < cont; i++)
			{ 
				int w = data[i].darIdVertice();
				if (!marked[w]) 
				{
					edgeTo[w] = v;
					marked[w] = true;
					distTo[w] = distTo[v] + 1;
					q.enqueue(w);
				}
			}
		}
	}

	private int masLejano(){
		int lejano = s;
		int dist = 0;
		for (int i = 0; i < distTo.length; i++){
			if (distTo[i] > dist){
				lejano = i;
				dist = distTo[i];
			}
			else if(distTo[i] == dist && i < lejano)
				lejano = i;
		}
		return lejano;
	}

	public Stack<InfoVertice> darCaminoMasLejano(){
		int vertMasLejano = masLejano();
		Stack<InfoVertice> path = new Stack<>();
		path.push(grafo.getInfoVertex(vertMasLejano));
		while (s != vertMasLejano){
			path.push(grafo.getInfoVertex(edgeTo[vertMasLejano]));
			vertMasLejano = edgeTo[vertMasLejano];
		}
		return path;
	}
}