package model.data_structures;

import java.util.Iterator;

import edu.princeton.cs.algs4.DirectedEdge;
import edu.princeton.cs.algs4.IndexMinPQ;
import model.data_structures.GrafNoDirigido;
import model.data_structures.SeparateChainingHash;
import model.data_structures.Stack;
import model.data_structures.LlavePos;

public class Dijkstra {
	private double[] distTo;          // distTo[v] = distance  of shortest s->v path
	private DirectedEdge[] edgeTo;    // edgeTo[v] = last edge on shortest s->v path
	private IndexMinPQ<Double> pq;    // priority queue of vertices

	private int[] llavesDelGrafo;
	private SeparateChainingHash<Integer, LlavePos> llavesHash;
	private boolean[] marcados;

	/**
	 * Computes a shortest-paths tree from the source vertex {@code s} to every other
	 * vertex in the edge-weighted digraph {@code G}.
	 *
	 * @param  G the edge-weighted digraph
	 * @param  s the source vertex
	 * @throws IllegalArgumentException if an edge weight is negative
	 * @throws IllegalArgumentException unless {@code 0 <= s < V}
	 */
	public Dijkstra(GrafNoDirigido pGrafo, int originVertex, int pInt){
		Iterable<Integer> llave = pGrafo.getVertices().keys();

		distTo = new double[pGrafo.V()];
		edgeTo = new DirectedEdge[pGrafo.V()];
		llavesDelGrafo = new int[pGrafo.V()];
		llavesHash = new SeparateChainingHash<Integer, LlavePos>(3);
		marcados = new boolean[pGrafo.V()];

		for (int v = 0; v < pGrafo.V(); v++)
			distTo[v] = Double.POSITIVE_INFINITY;

		int contador = 0;
		int contadorFijo = 0;
		Iterable<Integer> llaves = pGrafo.getVertices().keys();
		for(int laLlave : llaves){
			if(laLlave == originVertex){
				contadorFijo = contador;
			}
			llavesDelGrafo[contador] = laLlave;
			llavesHash.put(laLlave, new LlavePos(laLlave, contador));
			contador++;
		}

		// relax vertices in order of distance from s
		pq = new IndexMinPQ<Double>(pGrafo.V());
		pq.insert(contadorFijo, distTo[contadorFijo]);
		marcados[contadorFijo] = true;
		distTo[contadorFijo] = 0.0;
		
		while (!pq.isEmpty()){
			int v = pq.delMin();
			marcados[v] = true;
			int llavecitasDelGrafoABuscar = llavesDelGrafo[v];
			Iterator<Integer> g1 = pGrafo.adj(llavecitasDelGrafoABuscar);
			
			
			Iterator<Integer> g = pGrafo.adj(llavecitasDelGrafoABuscar);
			int cont = 0;
			while(g.hasNext()){
				int dato = g.next();
				LlavePos temp = llavesHash.get(dato);
				if(marcados[temp.getPos()] == false) {
					double costoArco=0;
					if(pInt==0) {
						costoArco = pGrafo.getCostArcTiempo(v, dato);
					}
					else if(pInt==1) {
						costoArco = pGrafo.getCostArcHar(v, dato);
					}
					
					DirectedEdge eje = new DirectedEdge(v, temp.getPos(), costoArco);
					relax(eje);
				}
			}
		}
	}

	// relax edge e and update pq if changed
	private void relax(DirectedEdge e) {
		int v = e.from(), w = e.to();
		if (distTo[w] > distTo[v] + e.weight())
		{
			distTo[w] = distTo[v] + e.weight();
			edgeTo[w] = e;

			if (pq.contains(w)){
				pq.decreaseKey(w, distTo[w]);
			}
			else{
				pq.insert(w, distTo[w]);
			}
		}
	}

	/**
	 * Returns the length of a shortest path from the source vertex {@code s} to vertex {@code v}.
	 * @param  v the destination vertex
	 * @return the length of a shortest path from the source vertex {@code s} to vertex {@code v};
	 *         {@code Double.POSITIVE_INFINITY} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public double distTo(int v) {
		return distTo[v];
	}

	/**
	 * Returns true if there is a path from the source vertex {@code s} to vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return {@code true} if there is a path from the source vertex
	 *         {@code s} to vertex {@code v}; {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public boolean hasPathTo(int v) {
		return distTo[v] < Double.POSITIVE_INFINITY;
	}

	/**
	 * Returns a shortest path from the source vertex {@code s} to vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return a shortest path from the source vertex {@code s} to vertex {@code v}
	 *         as an iterable of edges, and {@code null} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public Iterable<DirectedEdge> pathTo(int v) {
		if (!hasPathTo(v)) return null;
		Stack<DirectedEdge> path = new Stack<DirectedEdge>();
		for (DirectedEdge e = edgeTo[v]; e != null; e = edgeTo[e.from()]) {
			path.push(e);
		}
		return path;
	}

	public Stack<Integer> getEjes(int idVertice1, int idVertice2){
		boolean existe = false;
		int pos = 0;
		for(int i = 0; i < edgeTo.length; i++){
			if(edgeTo[i] != null && llavesDelGrafo[edgeTo[i].to()] == idVertice2){
				pos = i;
				existe = true;
			}
		}
		boolean estado2 = false;
		Stack<Integer> pila = new Stack<Integer>();
		int pos2 = pos;
		pila.push(llavesDelGrafo[edgeTo[pos].to()]);
		if(existe == true) {
			while(estado2 == false){
				pila.push(llavesDelGrafo[edgeTo[pos].from()]);
				if(llavesDelGrafo[edgeTo[pos].from()] == idVertice1){
					estado2 = true;
					break;
				}
				pos = edgeTo[pos].from();
			}
		}
		pila.push((int) distTo[pos2]);
		return pila;
	}

}