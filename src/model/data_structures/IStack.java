package model.data_structures;

public interface IStack<T> extends Iterable<T>{

	/**
	 * Retorna true si la pila est� vac�a
	 * @return true si la pila est� vac�a, false de lo contrario
	 */
	public boolean isEmpty();

	/**
	 * Retorna el n�mero de elementos contenidos
	 * @return el n�mero de elementos contenidos
	 */
	public int size();

	/**
	 * Inserta un nuevo elemento en la pila
	 * @param t el nuevo elemento que se va a agregar
	 */
	public void push(T t);

	/**
	 * Quita y retorna el elemento agregado mas recientemente
	 * @return el elemento agregado mas recientemente
	 */
	public T pop();	
}
