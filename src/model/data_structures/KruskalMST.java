package model.data_structures;

import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.UF;
import model.data_structures.Arco;
import model.data_structures.GrafNoDirigido;
import model.data_structures.Queue;
import model.data_structures.SeparateChainingHash;
import model.data_structures.InfoVertice;
import model.data_structures.Vertice;

public class KruskalMST {
	private double peso;                        // peso of MST
	private Queue<Arco> mst = new Queue<Arco>();  // edges in MST
	private int[] regreso;

	public KruskalMST(GrafNoDirigido pGrafo) {
		MinPQ<Arco> pq = new MinPQ<Arco>();
		Iterable<Integer> adyacentes = pGrafo.getArcos().keys();
		SeparateChainingHash<Integer, Integer> tablaHash = new SeparateChainingHash<Integer, Integer>(4);
		regreso = new int [pGrafo.E() + 1];

		for(int ady: adyacentes) {
			Queue<Arco> eje = (Queue<Arco>) pGrafo.getArcos().get(ady);
			for (Arco arc : eje) {
				pq.insert(arc);
			}
		}

		UF uf = new UF(pGrafo.V());
		while (!pq.isEmpty() && mst.size() < pGrafo.V() - 1) {
			Arco arco = pq.delMin();
			Vertice v = arco.darVer1();
			Vertice w = arco.other(v);
			if (!tablaHash.contains((int)v.darIdVertice())){
				regreso[tablaHash.getSize()] = (int) v.darIdVertice();
				tablaHash.put((int)v.darIdVertice(), tablaHash.getSize());
			}
			if(!tablaHash.contains((int)w.darIdVertice())) {
				regreso[tablaHash.getSize()] = 
						(int)w.darIdVertice();
				tablaHash.put((int) w.darIdVertice(), tablaHash.getSize());
			}
			if(!uf.connected(tablaHash.get((int) v.darIdVertice()), tablaHash.get((int) w.darIdVertice()))) {
				uf.union(tablaHash.get((int) v.darIdVertice()), tablaHash.get((int) w.darIdVertice()));
				mst.enqueue(arco);  // add edge e to mst
				peso += (Double) arco.darCostoHarv();
			}
		}
	}

	public Iterable<Arco> edges() {
		return mst;
	}

	public double getPeso() {
		return peso;
	}

}