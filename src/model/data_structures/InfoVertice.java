package model.data_structures;

public class InfoVertice {

	private int id;
	private double longitudVer;
	private double latitudVer;
	private int idZonaUber;
	
	public InfoVertice(int pId, double pLong, double pLati, int pZona) {
		id=pId;
		idZonaUber=pZona;
		latitudVer=pLati;
		longitudVer=pLong;
	}
	
	public int darId() {
		return id;
	}
	
	public double darLongitud() {
		return longitudVer;
	}
	
	public double darLatitud() {
		return latitudVer;
	}
	
	public int darZonaUber() {
		return idZonaUber;
	}
}
