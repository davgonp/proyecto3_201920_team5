package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Stack<T> implements IStack<T>{

	/**
	 * Primer nodo de la lista
	 */
	private Nodo<T> primero;

	/**
	 * Cantidad de elementos de la lista
	 */
	private int cantidadElementos;


	public boolean isEmpty(){
		return primero == null;
	}


	public int size(){
		return cantidadElementos;
	}

	
	public void push(T t){
		Nodo<T> viejoPrimero = primero;
		primero = new Nodo<T>(t);
		primero.cambiarSiguiente(viejoPrimero);
		cantidadElementos++;
	}

	
	public T pop(){
		T elim = primero.darElemento();
		primero = primero.darSiguiente();
		cantidadElementos--;
		return elim;
	}

	
	public Iterator<T> iterator(){
		return new IteratorLista<T>(primero);
	}
}
