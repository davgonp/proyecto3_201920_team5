package model.data_structures;

public class LlavePos {
	private int llave;
	private int pos;
	
	public LlavePos(int pLlave, int pPos) {
		llave = pLlave;
		pos = pPos;
	}
	
	public int getLlave() {
		return llave;
	}
	
	public int getPos(){
		return pos;
	}
}
