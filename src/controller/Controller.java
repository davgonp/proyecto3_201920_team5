package controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import model.data_structures.*;
import model.logic.MVCModelo;
import model.logic.Maps;
import model.logic.TravelTime;
import model.logic.Zona;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	private ArrayList<Nodo> nodos;

	private ArrayList<Zona> zonas;
	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;


		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();

			switch(option){
			case 1:
				System.out.println("Cargando datos");
				modelo.cargar();
				break;

			case 2:
				System.out.println("Cantidad componentes conectadas");
				System.out.println("El número de componentes conectadas es " + modelo.ccGrafo());
				break;

			case 3:
				System.out.println("Vertice más cercano");
				System.out.println("Ingrese longitud");
				double longi = lector.nextDouble();
				System.out.println("Ingrese latitud");
				double lat=lector.nextDouble();
				System.out.println("El id del vertice más cercano es: "+modelo.verticeMasCercano(longi, lat));

				break;

			case 4:
				System.out.println("Cargando archivo JSON");
				modelo.cargarJSON();
				break;

			case 5:
				System.out.println("Req. 1A");
				System.out.println("Ingrese longitud 1");
				double pLongOrg = lector.nextDouble();
				System.out.println("Ingrese latitud 1");
				double pLatOrg = lector.nextDouble();
				System.out.println("Ingrese longitud 2");
				double pLongDst = lector.nextDouble();
				System.out.println("Ingrese latitud 2");
				double pLatDst = lector.nextDouble();
				modelo.caminoMenorCostoHarvesine(pLongOrg, pLatOrg, pLongDst, pLatDst);
				break;

			case 6: 
				System.out.println("Req. 2A");
				System.out.println("Ingrese longitud 1");
				double pLongOrg2 = lector.nextDouble();
				System.out.println("Ingrese latitud 1");
				double pLatOrg2 = lector.nextDouble();
				System.out.println("Ingrese el tiempo");
				double data3 = lector.nextDouble();
				modelo.verticesAlcanzablesTiempo(pLongOrg2, pLatOrg2, data3);
				break;
			case 7: 
				System.out.println("Req. 3A");
				modelo.mstDistancia();
				break;
			case 8: 
				System.out.println("Req. 10"); 
				modelo.grafoZonas();
				break;
			case 9: 
				System.out.println("Req. 11");
				System.out.println("Ingrese una zona de origen (id)");
				int data1 = lector.nextInt();
				System.out.println("Ingrese una zona de destino (id)");
				int data2 = lector.nextInt();
				modelo.costoMinimoZonasTiempo(data1, data2);
				break;
			case 10: 
				System.out.println("Req. 12"); 
				System.out.println("Ingrese una zona de origen (id)");
				int data = lector.nextInt();
				modelo.caminoMasLargo(data);
				break;

			case 11: 
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;


			default: lector.close();
			System.out.println("--------- \n Opcion Invalida !! \n---------");
			break;
			}
		}

	}


}
